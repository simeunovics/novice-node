class Profiler {
  constructor(label) {
    this.label = label;
    this.lastTime = null;
  }
  start() {
    this.lastTime = process.hrtime();
  }
  end() {
    const [start, end] = process.hrtime(this.lastTime);

    console.log(
      `Timer "${this.label}" took ${start} seconds and ${end} nanoseconds.`,
    );
  }
}

module.exports = function(label) {
  if ('development' === process.env.NODE_ENV) {
    return new Profiler(label);
  }
  if ('production' === process.env.NODE_ENV) {
    return {
      start: () => {},
      end: () => {},
    };
  }

  throw new Error('Must st NODE_ENV');
};
