const ReadOnlyEventEmitter = require('./readOnlyEventEmitter');

const ticker = new ReadOnlyEventEmitter(emit => {
  let tickCount = 0;
  setInterval(() => emit('tick', tickCount++), 1000);
});

module.exports = ticker;
