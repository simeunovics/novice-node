function createProxy(subject) {
  const proto = Object.getPrototypeOf(subject);
  function Proxy(subject) {
    this.subject = subject;
  }
  Proxy.prototype = Object.create(proto);

  // proxied method
  Proxy.prototype.hello = function() {
    return this.subject.hello() + ' world';
  };

  // delegate method
  Proxy.prototype.goodbye = function() {
    return this.subject.goodbye.apply(this.subject, arguments);
  };

  return new Proxy();
}

// Using object literals
function createProxyObjectLiterals(subject) {
  return {
    hello: () => subject.hello() + ' world',
    goodbye: () => subject.goodbye.apply(subject, arguments),
  };
}

// Monkey patching
function createProxyMonkeyPatching(subject) {
  const helloOrig = subject.hello;
  subject.hello = () => helloOrig.call(this) + ' world';

  return subject;
}

module.exports = createProxy;
