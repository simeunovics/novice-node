const Koa = require('koa');
const app = new Koa();

app.use(function*() {
  this.body = { now: new Date() };
});
app.use(require('./rateLimit'));

app.listen(3000);
