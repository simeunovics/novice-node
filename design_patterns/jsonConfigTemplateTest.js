const JsonConfig = require('./jsonConfigTemplate');

const jsonConfig = new JsonConfig();
jsonConfig.read('samples/conf_mod.json');
jsonConfig.set('nodejs', 'design patterns');
jsonConfig.save('samples/conf_mod_template.json');
