const ConfigTemplate = require('./configTemplate');

class JsonConfigTemplate extends ConfigTemplate {
  _deserialize(data) {
    return JSON.parse(data);
  }

  _serialize(data) {
    return JSON.stringify(data, null, ' ');
  }
}

module.exports = JsonConfigTemplate;
