function createLoggingWritable(writableStream) {
  const proto = Object.getPrototypeOf(writableStream);

  function LoggingWritable(writableStream) {
    this.writableStream = writableStream;
  }
  LoggingWritable.prototype = Object.create(proto);

  LoggingWritable.prototype.write = function(chunk, encoding, callback) {
    if (callback && typeof encoding === 'function') {
      callback = encoding;
      encoding = undefined;
    }

    console.log('Writing ', chunk);
    return this.writableStream.write(chunk, encoding, function() {
      console.log('Finished writing ', chunk);
      callback && callback();
    });
  };
  LoggingWritable.prototype.on = function() {
    return this.writableStream.on.apply(this.writableStream, arguments);
  };
  LoggingWritable.prototype.end = function() {
    return this.writableStream.end.apply(this.writableStream, arguments);
  };

  return new LoggingWritable(writableStream);
}

const fs = require('fs');

const writable = fs.createWriteStream('test.txt');
const writableProxy = createLoggingWritable(writable);
writableProxy.write('This is the 1. chunk');
writableProxy.write('This is the 2. chunk');
writableProxy.write('This is the 3. chunk');
writable.write('This is not logged !!!');
writableProxy.end();
