const createFaisafeSocket = require('./failsaveSocket');
const failsaveSocket = createFaisafeSocket({ port: 5000 });

setInterval(() => {
  // send current memory usage
  failsaveSocket.send(process.memoryUsage());
}, 1000);
