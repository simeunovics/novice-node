module.exports = class OnlineState {
  constructor(socket) {
    this.socket = socket;
  }

  send(data) {
    this.socket.socket.write(data);
  }

  activate() {
    this.socket.queue.forEach(data => {
      this.socket.socket.write(data);
    });
    this.socket.queue = [];

    this.socket.socket.once('error', () => {
      this.socket.changeState('offline');
    });
  }
};
