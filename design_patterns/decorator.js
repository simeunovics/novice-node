function decorate(component) {
  const proto = Object.getPrototypeOf(component);

  function Decorator(component) {
    this.component = component;
  }
  Decorator.prototype = Object.create(proto);

  // new method
  Decorator.prototype.greeting = function() {
    return 'Hi!';
  };

  // delegate method
  Decorator.prototype.hello = function() {
    return this.component.apply(this.component, arguments);
  };

  return new Decorator(component);
}
