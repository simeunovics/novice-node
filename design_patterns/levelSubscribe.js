module.exports = function(db) {
  db.subscribe = (pattern, listener) => {
    db.on('put', (key, val) => {
      const match = Object.keys(pattern).every(
        index => pattern[index] === val[index],
      );

      if (match) {
        listener(key, val);
      }
    });
  };
  return db;
};
