function loadModule(filename, module, require) {
    const wrapperSrc = `(function (module, exports, require) {
        ${fs.readFileSync(filename, 'utf8')}
    })(module, module.exports, require)
    `;
    eval(wrapperSrc);
}

const require = moduleName => {
    const id = require.resolve(moduleName);
    if (require.cache[id]) {
        return require.cache[id].exports;
    }

    const module = {
        exports: {},
        id: id,
    };

    require.cache[id] = module;
    loadModule(id, module, require);

    return module.exports;
};

require.cache = {};
require.resolve = moduleName => {
    // todo
};
