const stream = require('stream');

class LimitedParallelStream extends stream.Transform {
  constructor(concurency, userTransform) {
    super({ objectMode: true });
    this.concurency = concurency;
    this.userTransform = userTransform;
    this.running = 0;
    this.terminateCallback = null;
    this.continueCallback = null;
  }
  _transform(chunk, enc, done) {
    this.running++;
    this.userTransform(
      chunk,
      enc,
      this.push.bind(this),
      this._onComplete.bind(this),
    );
    if (this.running < this.concurency) {
      done();
    } else {
      this.continueCallback = done;
    }
  }
  _flush(done) {
    if (this.running === 0) {
      return done();
    }

    this.terminateCallback = done;
  }
  _onComplete(err) {
    this.running--;
    if (err) {
      return this.emit('error', err);
    }
    const tmpCallback = this.continueCallback;
    this.continueCallback = null;
    tmpCallback && tmpCallback();
    if (this.running === 0) {
      this.terminateCallback && this.terminateCallback();
    }
  }
}

module.exports = LimitedParallelStream;
