const child_process = require('child_process');
const net = require('net');

function multiplexChannels(source, destination) {
  let totalChannels = source.length;
  for (let i = 0; i < totalChannels; i++) {
    source[i]
      .on('readable', function () {
        let chunk;
        while ((chunk = this.read()) !== null) {
          const outBuff = new Buffer(1 + 3 + chunk.length);
          outBuff.writeUInt8(i, 0);
          outBuff.writeUInt32BE(chunk.length, 1);
          chunk.copy(outBuff, 5);
          console.log('Sending packet to channel: ' + i);
          destination.write(outBuff);
        }
      })
      .on('end', () => {
        if (--totalChannels === 0) {
          destination.end();
        }
      });
  }
}

const socket = net.connect(
  3000,
  () => {
    const child = child_process.fork(process.argv[2], process.argv.slice(3), {
      silent: true,
    });
    multiplexChannels([child.stdout, child.stderr], socket);
  },
);
