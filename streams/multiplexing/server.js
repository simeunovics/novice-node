const net = require('net');
const fs = require('fs');

function demultiplexChannel(source, destinations) {
  let currentChannel = null;
  let currentLength = null;

  source
    .on('readable', () => {
      let chunk;
      if (null === currentChannel) {
        chunk = source.read(1);
        currentChannel = chunk && chunk.readUInt8(0);
      }

      if (null === currentLength) {
        chunk = source.read(4);
        currentLength = chunk && chunk.readUInt32BE(0);
      }

      if (null === currentLength) {
        return;
      }

      chunk = source.read(currentLength);
      if (null === chunk) {
        return;
      }
      console.log('Received packet from: ' + currentChannel);
      destinations[currentChannel].write(chunk);
      currentLength = null;
      currentChannel = null;
    })
    .on('end', () => {
      destinations.forEach(destination => destination.end());
      console.log('Source channel closed');
    });
}

net
  .createServer(socket => {
    const stdoutStream = fs.createWriteStream('stdout.log');
    const stderrStream = fs.createWriteStream('stderr.log');
    demultiplexChannel(socket, [stdoutStream, stderrStream]);
  })
  .listen(3000, () => console.log('Searver started!'));
