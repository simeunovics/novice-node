const Chance = require('chance');
const chance = new Chance();

// Without back presure
require('http')
  .createServer((req, res) => {
    res.writeHead(200, { 'Content-Type': 'text/plain' });
    while (chance.bool({ likelihood: 99 })) {
      res.write(chance.string() + '\n');
    }
    res.end('The end...\n');
    res.on('finish', () => console.log('All data was sent'));
  })
  .listen(1111, () => console.log('Listening...'));

// With back presure
require('http')
  .createServer((req, res) => {
    res.writeHead(201, { 'Content-Type': 'text/plain' });
    function generateMore() {
      while (chance.bool({ likelihood: 95 })) {
        let shouldContinue = res.write(
          chance.string({ length: 16 * 1024 - 1 }),
        );
        if (!shouldContinue) {
          console.log('Backpresure');
          return res.once('drain', generateMore);
        }
      }
      res.end('The end...', () => console.log('All data was sent'));
    }
    generateMore();
  })
  .listen(1111, () => console.log('Listening...'));
