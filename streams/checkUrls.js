const fs = require('fs');
const split = require('split');
const through2 = require('through2');
const throughParallel = require('through2-parallel');
const request = require('request');
const ParallelStream = require('./parallelStream');

const sequential = through2.obj(function(url, enc, done) {
  if (!url) return done();
  request.head(url, (err, response) => {
    this.push(url + ' is ' + (err ? 'down' : 'up') + '\n');
    done();
  });
});

const parallel = new ParallelStream((url, enc, push, done) => {
  if (!url) return done();
  request.head(url, (err, response) => {
    push(url + ' is ' + (err ? 'down' : 'up') + '\n');
    done();
  });
});

const concurentParallel = throughParallel.obj({ concurency: 2 }, function(
  url,
  enc,
  done,
) {
  if (!url) return done();
  request.head(url, (err, response) => {
    this.push(url + ' is ' + (err ? 'down' : 'up') + '\n');
    done();
  });
});

let engine;
switch (process.argv[3]) {
  case 'seq':
    engine = sequential;
    break;
  case 'par':
    engine = parallel;
    break;
  default:
    engine = concurentParallel;
    break;
}

fs.createReadStream(process.argv[2])
  .pipe(split())
  .pipe(engine)
  .pipe(fs.createWriteStream('results.txt'))
  .on('finish', () => console.log('All URLs were checked'));
