let word = '';
function* fruitGenerator() {
    yield 'apple';
    console.log('word is: '+word);
    yield 'orange';
    console.log('word is: '+word);
    yield 'watermelon';
    console.log('word is: '+word);
}

const newFruitGenerator = fruitGenerator();
console.log(word = newFruitGenerator.next().value);
console.log('1');
console.log(word = newFruitGenerator.next().value);
console.log('2');
console.log(word = newFruitGenerator.next().value);
console.log('3');
console.log(word = newFruitGenerator.next().value);
