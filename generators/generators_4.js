function asyncFlow(generatorFunction) {
    function callback(err) {
        if (err) {
            return generator.throw(err);
        }
        const args = [].slice.call(arguments, 1);
        console.log(args);
        generator.next(args.length > 1 ? args : args[0]);
    }
    const generator = generatorFunction(callback);
    generator.next();
}
