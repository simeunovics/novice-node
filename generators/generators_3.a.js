function* twoWayGenerator() {
    const what = yield null;
    console.log('Hello ' + what);
}
const twoWay = twoWayGenerator();

try {
    twoWay.next();
    twoWay.next('world');
    twoWay.throw(new Error('Piss'));
} catch (e) {
    console.log(e.message);
}
