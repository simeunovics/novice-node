const express = require('express');
const bodyParser = require('body-parser');
const errorHandler = require('errorhandler');
const http = require('http');

const serviceLocator = require('./lib/serviceLocator');
serviceLocator.register('dbName', 'example-db');
serviceLocator.register('tokenSecret', 'SHHH!');
serviceLocator.factory('db', require('./lib/db'));
serviceLocator.factory('authService', require('./lib/authService'));
serviceLocator.factory('authController', require('./lib/authController'));

const authController = serviceLocator.get('authController');

const app = (module.exports = express());
app.use(bodyParser.json());

app.post('/login', authController.login);
app.get('/checkToken', authController.checkToken);

app.use(errorHandler());

http.createServer(app).listen(3000, () => {
  console.log('Express server started');
});
