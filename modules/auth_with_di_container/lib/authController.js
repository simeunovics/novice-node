const authService = require('./authService');

module.exports = serviceLocator => {
  const authService = serviceLocator.get('authService');
  const authController = {};

  authController.login = (req, res, next) => {
    authService.login(
      req.body.username,
      req.body.password,
      (err, result) => {},
    );
  };
  authController.checkToken = (req, res, next) => {
    authService.checkToken(req.query.token, (err, result) => {});
  };

  return authController;
};
