const db = require('aDb');

module.exports = function findAll(type, callback) {
  if (db.connected) {
    runFind();
  } else {
    db.once('connected', runFind);
  }
  function runFind() {
    db.findAll(type, callback);
  }
};
