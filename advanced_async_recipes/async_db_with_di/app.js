const db = require('aDb');
const findAllFactory = require('./findAll');

db.once('connected', function() {
  const findAll = findAllFactory(db);
})