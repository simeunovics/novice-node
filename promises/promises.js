module.exports.promisify = function(callbackBasedApi) {
    return function promisified() {
        const args = [].slice.call(arguments);
        return new Promise((resolve, reject) => {
            args.push((err, result) => {
                if (err) {
                    return reject(err);
                }
                if (arguments.length < 2) {
                    resolve(result);
                } else {
                    resolve([].slice.call(arguments, 1));
                }
            });
            callbackBasedApi.apply(null, args);
        });
    };
};

const tasks = [
    () => setTimeout(() => console.log(100), 100),
    () => setTimeout(() => console.log(400), 400),
    () => setTimeout(() => console.log(300), 300),
    () => setTimeout(() => console.log(500), 500),
    () => setTimeout(() => console.log(800), 800),
];

const allTasks = tasks.reduce((prev, task) => {
    console.log(prev);
    console.log('Dispatching task');
    return prev.then(() => task());
}, Promise.resolve());

allTasks.then(() => console.log('Con'));
allTasks.then(() => console.log('Done'));