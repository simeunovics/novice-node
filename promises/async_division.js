const async_division = function(divident, divisor, cb) {
    return new Promise((resolve, reject) => {
        process.nextTick(() => {
            const result = divident / divisor;
            if (isNaN(result) || !Number.isFinite(result)) {
                const err = new Error('Invalid operands');
                if (cb) cb(err);
                return reject(err);
            }
            if (cb) cb(null, result);
            resolve(result);
        });
    });
};

async_division(5, 5).then(res => console.log(res));
async_division(5, 5, res => console.log(res));
