const EventEmiter = require('events').EventEmitter;
const fs = require('fs');

function findPattern(files, pattern) {
    const emitter = new EventEmiter();
    files.forEach(function(file) {
        fs.readFile(file, 'utf8', (err, content) => {
            if (err) return emitter.emit('error', err);

            emitter.emit('fileread', file);
            let match;
            if ((match = content.match(pattern))) {
                match.forEach(elem => emitter.emit('found', file, elem));
            }
        });
    });
    return emitter;
}

findPattern(['fileA.txt', 'fileB.txt'], /hello \w+/g)
    .on('filtered', file => console.log(file + ' was read'))
    .on('found', (file, match) =>
        console.log("Matcher '" + match + "' in file " + file),
    )
    .on('error', err => console.log('Error emitted: ', err.message));
