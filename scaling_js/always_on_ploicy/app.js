const http = require('http');
const pid = process.pid;

http
  .createServer((req, res) => {
    for (let i = 90000; i > 0; i--) {
      console.log(`Handing ${i} request from pid ${pid}`);
    }
    res.end(`Hello from ${pid}`);
  })
  .listen(8080, () => console.log(`Started ${pid}`));
