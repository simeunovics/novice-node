class TaskQueue {
    constructor(concurency) {
        this.concurency = concurency;
        this.runnunig = 0;
        this.queue = [];
    }

    pushTask(task) {
        this.queue.push(task);
        this.next();
    }

    next() {
        while (this.runnunig < this.concurency && this.queue.length) {
            const task = this.queue.shift();
            task(() => {
                this.runnunig--;
                this.next();
            });
            this.runnunig++;
        }
    }
}
